﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  
using UnityEngine.EventSystems; 
using System.IO; 
using UnityEngine.Sprites; 

public class Slots : MonoBehaviour {
    
    public Reel[] reel; 
    bool startSpinning; 
    bool bonusSpin; 
    public GameObject MoneyCanvas;
    public GameObject Cherry1, Cherry2, Cherry3, Diamond1, Diamond2, Diamond3, Seven1, Seven2, Seven3;  
    public bool match = false; 
    public bool bonusmatch = false; 
    public GameObject WinCanvas; 
    public GameObject BonusCanvas; 


    private void Start() {
        startSpinning = false; 
        bonusSpin = false; 
        bonusmatch = false; 
        match = false;
        Debug.Log("start works");
    }

    private void Update() {
        if (!startSpinning) {  //can't spin if its already spinning 
            if (Input.GetKeyDown(KeyCode.S)) {  //starts slot machine 
                startSpinning = true; 
                WinCanvas.SetActive(false); 
                playerBetSpin (); 
                StartCoroutine (Spinning()); 
                Debug.Log("can start spin"); 
            }
        }
        if (!bonusSpin) {  //can't spin if its already spinning 
            if (Input.GetKeyDown(KeyCode.B)) {  //starts slot machine 
                bonusSpin = true; 
                BonusCanvas.SetActive(false); 
                playerBonusSpin (); 
                StartCoroutine (BonusSpinning()); 
                Debug.Log("can start bonus spin");
            }
        } 
    }

    IEnumerator Spinning () {
        foreach (Reel spinner in reel) {
            spinner._spinTheSlots =true;   // spins the reels
        }
        for(int i = 0; i < reel.Length; i++) {
            yield return new WaitForSeconds(Random.Range(1, 3));  //random amount of time for spins and stopping 
            reel[i]._spinTheSlots = false;
            reel[i].RandomisePosition(); 
        }
        CheckReels ();  
        startSpinning = false; //machine can start again
    }

    IEnumerator BonusSpinning () {
        foreach (Reel spinner in reel) {
            spinner._spinTheSlots =true;   // spins the reels
        }
        for(int i = 0; i < reel.Length; i++) {
            yield return new WaitForSeconds(Random.Range(1, 3));  //random amount of time for spins and stopping 
            reel[i]._spinTheSlots = false;
            reel[i].RandomisePosition(); 
        }
        CheckBonusReels (); 
        bonusSpin = false; //machine can start again 
    } 

    void CheckReels () {
        Debug.Log("can check reels");

        if ((Cherry1.transform.position.y == Cherry2.transform.position.y) && (Cherry2.transform.position.y == Cherry3.transform.position.y) && (Cherry3.transform.position.y == Cherry1.transform.position.y)) {
            playerWonSpin ();
            WinBanner ();
            Debug.Log("yass queen1");
        } 
        else {
            match = false;
        }
        if ((Diamond1.transform.position.y == Diamond2.transform.position.y) && (Diamond2.transform.position.y == Diamond3.transform.position.y) && (Diamond3.transform.position.y == Diamond1.transform.position.y)) {
            playerWonSpin ();
            WinBanner ();
            Debug.Log("yass queen2");
        } 
        else {
            match = false;
        }
        if ((Seven1.transform.position.y == Seven2.transform.position.y) && (Seven2.transform.position.y == Seven3.transform.position.y) && (Seven3.transform.position.y == Seven1.transform.position.y)) {
            playerWonSpin ();
            WinBanner (); 
            Debug.Log("yass queen3");
        } 
        else {
            match = false;
        }
    }
     void CheckBonusReels () {
        Debug.Log("can check reels");

        if ((Cherry1.transform.position.y == Cherry2.transform.position.y) && (Cherry2.transform.position.y == Cherry3.transform.position.y) && (Cherry3.transform.position.y == Cherry1.transform.position.y)) {
            BigBonusWin ();
            BonusBanner (); 
            Debug.Log("yass queen1");
        } 
        else {
            bonusmatch = false; 
        }
        if ((Diamond1.transform.position.y == Diamond2.transform.position.y) && (Diamond2.transform.position.y == Diamond3.transform.position.y) && (Diamond3.transform.position.y == Diamond1.transform.position.y)) {

            BigBonusWin ();
            BonusBanner (); 
            Debug.Log("yass queen2");
        } 
        else {
            bonusmatch = false; 
        }
        if ((Seven1.transform.position.y == Seven2.transform.position.y) && (Seven2.transform.position.y == Seven3.transform.position.y) && (Seven3.transform.position.y == Seven1.transform.position.y)) { 
            BigBonusWin ();
            BonusBanner (); 
            Debug.Log("yass queen3");
        } 
        else {
            bonusmatch = false; 
        }
    }

    void WinBanner (){
        WinCanvas.SetActive(true); 
        match = true; 
    }
    void BonusBanner(){
        BonusCanvas.SetActive(true); 
        bonusmatch = true; 
    }
    void playerBetSpin () {
        MoneyCanvas.GetComponent<Monies> ().betMoney (100); 
    }
    void playerBonusSpin () {
        MoneyCanvas.GetComponent<Monies> ().BonusSpin (1000); 
    }
    void playerWonSpin () {
        MoneyCanvas.GetComponent<Monies> ().winMoney (200); 
    }
    void BigBonusWin () {
        MoneyCanvas.GetComponent<Monies> ().BonusWin (3000);
    }
     
}