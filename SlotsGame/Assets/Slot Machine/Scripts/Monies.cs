﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Monies : MonoBehaviour {
    
    public int money; 
    public Text bankText; 

    void Start() {
        money = 10000; 
        bankText.text = money.ToString (); 
    }

    public void betMoney (int minusMoney) {
        if (money - minusMoney < 0) {
            Debug.Log("No Funds");
        }
        else {
            money -= minusMoney; 
            bankText.text = money.ToString (); 
        }
    }
    public void winMoney (int addMoney) {
        money += addMoney; 
        bankText.text = money.ToString (); 
    }
    public void BonusSpin (int minusMoney) {
        if (money - minusMoney < 0) {
            Debug.Log("Not enough funds");
        }
        else {
            money -= minusMoney;
            bankText.text = money.ToString (); 
        }
    }
    public void BonusWin (int addMoney) {
        money += addMoney; 
        bankText.text = money.ToString (); 
    }
}
