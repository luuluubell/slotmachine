﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reel : MonoBehaviour {
    
    public bool _spinTheSlots; //control when the reel spins
    int _speed; 

    private void Start() {
        _spinTheSlots = false;
        _speed = 2000; 
    }

    void Update() {
        if (_spinTheSlots) {

            foreach (Transform image in transform) { 
                image.transform.Translate(Vector3.down * Time.smoothDeltaTime * _speed, Space.World); //direction and speed

                if (image.transform.position.y <= 0) {
                    image.transform.position = new Vector3 (image.transform.position.x, image.transform.position.y + 600, image.transform.position.z); 
                }
            }
        }
    }

    public void RandomisePosition () { //to randomise the positions of the images

        List<int> parts = new List<int> ();
            //original Y positions will be used to randomise new positions
        parts.Add(250);
        parts.Add(0);
        parts.Add(-250);
        
        foreach (Transform image in transform) {
            int randomisation = Random.Range(0, parts.Count); 
            image.transform.position = new Vector3(image.transform.position.x, parts[randomisation] + transform.parent.GetComponent<RectTransform>().transform.position.y, image.transform.position.z); 
                                                                                                             //above adjusts it to the Y on the canvas position 
            parts.RemoveAt(randomisation); 
        }
    }

}