# SlotMachine

This contains the basic framework for a slot machine game.

The rules of a slot machine are simple;
You bet a set amount of money on a spin which will decrease your bank.
There's also the option for a player to bet on a bonus spin which will use more money to bet, but if the spin is successful the player gets tripple the bet amount. 
All bet and win ratios are the same and are set. 
A player wins when the row of reels contains the same icon. 
Whether a player wins or loses doesn't determine whether they can spin again.
If the player wins a spin a win banner shows above the slot. 

The Reel code controls the reels of the slot machine.
The reels are spinned at a specific rate set by the developer. This is changeable.
Each of the icons has a set Y position, this can be changed as well, but can change the visuals of the game. Caution should be taken when changing this. 
The icons only spin on the Y axis and are fixed in the Z and X axis. 

The Slots code controls the spin of the slots. 
The spin is initiated with a button press, like most slot machines. In this case its the space bar. 
The spin is limited so that the player cannot contually press the button and cause the machine to continuously spin. 
Once the spin is completed the player is allowed to press the button again. 
The reels spin for a random amount of time in a certain range of seconds, this can be adjusted accordingly in this code.

Art can be changed to your preference with your own assets. 
There are comments in the script explaining what certain parts of the code do and why they are there to aid those that use this and don't understand what the code means or does. 